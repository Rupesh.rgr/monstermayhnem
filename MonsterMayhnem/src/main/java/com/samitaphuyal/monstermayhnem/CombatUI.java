/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.samitaphuyal.monstermayhnem;

import com.samitaphuyal.monstermayhnem.model.BaseClass;
import com.samitaphuyal.monstermayhnem.model.Monster;
import com.samitaphuyal.monstermayhnem.model.Player;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Acer
 */
public class CombatUI extends javax.swing.JFrame {

    List<BaseClass> allPlayers = new ArrayList<>();
    List<BaseClass> suffledList = new ArrayList<>();
    List<String> turn;
    int turnCounter =0;
    
    public CombatUI() {
        initComponents();
    }

    public CombatUI(List<Player> playerList, List<Monster> selectedMonsterList) {
        initComponents();
        allPlayers.addAll(playerList);
        allPlayers.addAll(selectedMonsterList);
        suffleTurn();
        showCurrentStats();
        fillList();
    }
    
    public void suffleTurn(){
         suffledList = new ArrayList<>();
         turn = new ArrayList<>();
         for(int i=0; i< allPlayers.size();i++){
             for(int j=0; j< allPlayers.get(i).getSpeed();j++){
                 suffledList.add(allPlayers.get(i));
             }
         }
         Collections.shuffle(suffledList, new Random());
         for(int i=0;i<suffledList.size();i++){
             turn.add(suffledList.get(i).getName());
         }
         String str=String.join(", ",turn);
         turnDisplay.setText(String.valueOf(str));
         
    }
     public void checkWin(){
        int playerSize = 0;
        int monsterSize = 0;
        for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getPlayerOrMonster() == 1){
                playerSize++;
            }
            if(allPlayers.get(i).getPlayerOrMonster() == 2){
                monsterSize++;
            }
        }
        if(playerSize ==0){
             JOptionPane.showMessageDialog(this, "OOPS !! Monster Wins !!. All Players are Dead. Click Ok to Play Again");
              close();
              MainUI mainUI = new MainUI();
              mainUI.setVisible(true);
        }
        if(monsterSize ==0){
              JOptionPane.showMessageDialog(this, "Hurray !! Player Wins !!. All Monsters are Dead. Click Ok to Play Again");
              close();
              MainUI mainUI = new MainUI();
              mainUI.setVisible(true);    
        }
    }
    
    public void showCurrentStats(){
        BaseClass currentStats = suffledList.get(turnCounter);
        currentTurn.setText(currentStats.getName());
        currentPower.setText(String.valueOf(currentStats.getPower()));
        currentLife.setText(String.valueOf(currentStats.getLife()));
        currentSpeed.setText(String.valueOf(currentStats.getSpeed()));
        currentWeaponOrType.setText(String.valueOf(currentStats.getTypeOrWeapon()));
        currentHealth.setText(String.valueOf(currentStats.getHealth()));
        if(currentStats.getPlayerOrMonster() == 1){
        weaponOrType.setText("Weapon");
        PorM.setText("Player");
        }else{
            weaponOrType.setText("Type");
            PorM.setText("Monster");
        }
    }
    
    
    public void fillList(){
        checkWin();
        DefaultListModel model1 = new DefaultListModel();
        DefaultListModel model2 = new DefaultListModel();
        BaseClass attacker = suffledList.get(turnCounter);
        if(attacker.getHealth() <1){
            attackTab.setText(attacker.getName()+" is Dead !! Press Next");
            HealList.setVisible(false);
            attackList.setVisible(false);
            healTab.setText(attacker.getName()+" is Dead !! Press Next");
            powerUpTab.setText(attacker.getName()+" is Dead !! Press Next");
            healButton.setVisible(false);
            powerUpButton.setVisible(false);
            attackButton.setVisible(false);
            nextTurn.setVisible(true);
            nextTurn1.setVisible(true);
            nextTurn2.setVisible(true);
        }else if(suffledList.get(turnCounter).getPlayerOrMonster() == 1){
              attackTab.setText("Choose a Monster to Attack !!");
        for(int i=0; i< allPlayers.size();i++){
            if(allPlayers.get(i).getPlayerOrMonster() ==2){
                if(allPlayers.get(i).getHealth() > 0){
            model1.addElement(allPlayers.get(i).getName());
                }
        }
        }
        for(int i=0; i< allPlayers.size();i++){
            if(allPlayers.get(i).getPlayerOrMonster() ==1){
                 if(allPlayers.get(i).getHealth() > 0){
            model2.addElement(allPlayers.get(i).getName());
                 }
        }   
        }
        attackList.setVisible(true);
        attackButton.setVisible(true);
        HealList.setVisible(true);
        healButton.setVisible(true);
        powerUpButton.setVisible(true);
        nextTurn.setVisible(false);
        nextTurn1.setVisible(false);
        nextTurn2.setVisible(false);
        healTab.setText("Choose a Player to Heal");
        powerUpTab.setText("Increase your Power");
        attackList.setModel(model1);
        HealList.setModel(model2);
        }
        else if(suffledList.get(turnCounter).getPlayerOrMonster() == 2){
         for(int i=0; i< allPlayers.size();i++){
            if(allPlayers.get(i).getPlayerOrMonster() == 1){
                 if(allPlayers.get(i).getHealth() > 0){
            model1.addElement(allPlayers.get(i).getName());
            attackTab.setText("Choose a Player to Attack !!");
            HealList.setVisible(false);
            healTab.setText("Monster can only Attack");
            powerUpTab.setText("Monster can only Attack");
            healButton.setVisible(false);
            powerUpButton.setVisible(false);
            nextTurn.setVisible(false);
            nextTurn1.setVisible(false);
            nextTurn2.setVisible(false);
            attackList.setVisible(true);
            attackButton.setVisible(true);
                 }
                }
             }
         attackList.setModel(model1);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        turnDisplay = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        currentTurn = new javax.swing.JTextField();
        currentPower = new javax.swing.JTextField();
        currentLife = new javax.swing.JTextField();
        currentSpeed = new javax.swing.JTextField();
        currentWeaponOrType = new javax.swing.JTextField();
        currentHealth = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        weaponOrType = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        attackButton = new javax.swing.JButton();
        attackTab = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        attackList = new javax.swing.JList<>();
        nextTurn = new javax.swing.JButton();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        healButton = new javax.swing.JButton();
        healTab = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        HealList = new javax.swing.JList<>();
        nextTurn1 = new javax.swing.JButton();
        jLayeredPane3 = new javax.swing.JLayeredPane();
        powerUpButton = new javax.swing.JButton();
        powerUpTab = new javax.swing.JLabel();
        nextTurn2 = new javax.swing.JButton();
        PorM = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("MonsterMayhnem - Combat");

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel1.setText("Combat");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Turn");

        turnDisplay.setEditable(false);

        jLabel3.setText("Current Turn");

        currentPower.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                currentPowerActionPerformed(evt);
            }
        });

        jLabel4.setText("Power");

        jLabel5.setText("Life");

        jLabel6.setText("Speed");

        jLabel8.setText("Health");

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        jTabbedPane1.setBackground(new java.awt.Color(102, 51, 0));

        attackButton.setBackground(new java.awt.Color(0, 102, 255));
        attackButton.setText("Attack");
        attackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attackButtonActionPerformed(evt);
            }
        });

        attackList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(attackList);

        nextTurn.setBackground(new java.awt.Color(204, 0, 204));
        nextTurn.setText("Next Turn");
        nextTurn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextTurnActionPerformed(evt);
            }
        });

        jLayeredPane1.setLayer(attackButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(attackTab, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(nextTurn, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(195, 195, 195)
                        .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(nextTurn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(attackButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(attackTab, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(58, Short.MAX_VALUE))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(attackTab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(nextTurn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(attackButton)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Attack", jLayeredPane1);

        healButton.setBackground(new java.awt.Color(0, 153, 102));
        healButton.setText("Heal");
        healButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                healButtonActionPerformed(evt);
            }
        });

        HealList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(HealList);

        nextTurn1.setBackground(new java.awt.Color(204, 0, 204));
        nextTurn1.setText("Next Turn");
        nextTurn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextTurn1ActionPerformed(evt);
            }
        });

        jLayeredPane2.setLayer(healButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(healTab, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(nextTurn1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addGap(195, 195, 195)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nextTurn1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(healButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                .addGap(0, 52, Short.MAX_VALUE)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(healTab, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(62, 62, 62))
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(healTab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(nextTurn1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(healButton)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Heal", jLayeredPane2);

        powerUpButton.setBackground(new java.awt.Color(255, 0, 51));
        powerUpButton.setFont(new java.awt.Font("Sitka Display", 3, 18)); // NOI18N
        powerUpButton.setText("Power Up");
        powerUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                powerUpButtonActionPerformed(evt);
            }
        });

        nextTurn2.setBackground(new java.awt.Color(204, 0, 204));
        nextTurn2.setText("Next Turn");
        nextTurn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextTurn2ActionPerformed(evt);
            }
        });

        jLayeredPane3.setLayer(powerUpButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(powerUpTab, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(nextTurn2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane3Layout = new javax.swing.GroupLayout(jLayeredPane3);
        jLayeredPane3.setLayout(jLayeredPane3Layout);
        jLayeredPane3Layout.setHorizontalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane3Layout.createSequentialGroup()
                .addGroup(jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(powerUpButton, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jLayeredPane3Layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(powerUpTab, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(76, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(nextTurn2)
                .addGap(199, 199, 199))
        );
        jLayeredPane3Layout.setVerticalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(powerUpTab, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(powerUpButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nextTurn2)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Power Up", jLayeredPane3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(weaponOrType, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3)
                                .addGap(20, 20, 20)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(currentTurn, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(currentHealth, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                                .addComponent(currentWeaponOrType, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(currentLife, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(currentPower, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(currentSpeed, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(PorM, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(turnDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 671, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(59, 59, 59))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(357, 357, 357))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(turnDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(PorM, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(currentTurn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 242, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(currentHealth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(25, 25, 25))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(currentPower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4))
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(currentLife, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(currentSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6))
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(currentWeaponOrType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(weaponOrType, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(63, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void nextTurnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextTurnActionPerformed
        turnCounter++;
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
    }//GEN-LAST:event_nextTurnActionPerformed

    private void currentPowerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_currentPowerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_currentPowerActionPerformed

    private void powerUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_powerUpButtonActionPerformed
        if(suffledList.get(turnCounter).getSpeed()<=1){
              JOptionPane.showMessageDialog(this,"OOPS!! Speed should be greater than 1 for Power Up!!");
        }else{
            BaseClass player = suffledList.get(turnCounter);
            powerUp(player);
        turnCounter++;
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         checkHealth();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
            
        }
    }//GEN-LAST:event_powerUpButtonActionPerformed

    private void attackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_attackButtonActionPerformed
        String attackerName = suffledList.get(turnCounter).getName();
        BaseClass attacker = suffledList.get(turnCounter);
        String toAttackName = "";
        if(attackList.getSelectedValue() == null){
            JOptionPane.showMessageDialog(this,"Please select someone to attack !!");
        }else{
        toAttackName = attackList.getSelectedValue(); 
        BaseClass toAttack = findToAttack(toAttackName);
        attack(attacker,toAttack);
        turnCounter++;
        checkHealth();
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
        }
    }//GEN-LAST:event_attackButtonActionPerformed

    
     public BaseClass findToAttack(String name){
        BaseClass toAttack = new BaseClass();
        for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getName().equals(name)){
               toAttack.setId(allPlayers.get(i).getId());
               toAttack.setName(allPlayers.get(i).getName());
               toAttack.setLife(allPlayers.get(i).getLife());
               toAttack.setPower(allPlayers.get(i).getPower());
               toAttack.setSpeed(allPlayers.get(i).getSpeed());
               toAttack.setTypeOrWeapon(allPlayers.get(i).getTypeOrWeapon());
               toAttack.setHealth(allPlayers.get(i).getHealth());
               toAttack.setPlayerOrMonster(allPlayers.get(i).getPlayerOrMonster());
            }
        }
        return toAttack;
    }
     
    private void healButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_healButtonActionPerformed
        String healerName = suffledList.get(turnCounter).getName();
        BaseClass healer = suffledList.get(turnCounter);
        String toHealName = "";
        if(HealList.getSelectedValue() == null){
            JOptionPane.showMessageDialog(this,"Please select someone to heal !!");
        }else{
        toHealName = HealList.getSelectedValue(); 
        BaseClass toHeal = findToAttack(toHealName);
        heal(healer,toHeal);
        
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         checkHealth();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
        }
    }//GEN-LAST:event_healButtonActionPerformed

    private void nextTurn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextTurn1ActionPerformed
        turnCounter++;
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
    }//GEN-LAST:event_nextTurn1ActionPerformed

    private void nextTurn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextTurn2ActionPerformed
        turnCounter++;
        if(turnCounter < suffledList.size()){
         showCurrentStats();
         fillList();
        }else{
            checkWin();
            JOptionPane.showMessageDialog(this,"All turn finished. Suffling Again !!!");
            turnCounter = 0;
            suffleTurn();
            showCurrentStats();
            fillList();
        }
    }//GEN-LAST:event_nextTurn2ActionPerformed

      
    public void checkHealth(){
           String playerOrMonster = "";
             for(int i =0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getHealth() < 1){
                if(allPlayers.get(i).getPlayerOrMonster() == 1){
                    playerOrMonster = "Player";
                }else{
                    playerOrMonster = "Monster";
                }
                 JOptionPane.showMessageDialog(this,playerOrMonster+" "+ allPlayers.get(i).getName()+" is dead.");
                 allPlayers.remove(i);
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CombatUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CombatUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CombatUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CombatUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CombatUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> HealList;
    private javax.swing.JLabel PorM;
    private javax.swing.JButton attackButton;
    private javax.swing.JList<String> attackList;
    private javax.swing.JLabel attackTab;
    private javax.swing.JTextField currentHealth;
    private javax.swing.JTextField currentLife;
    private javax.swing.JTextField currentPower;
    private javax.swing.JTextField currentSpeed;
    private javax.swing.JTextField currentTurn;
    private javax.swing.JTextField currentWeaponOrType;
    private javax.swing.JButton healButton;
    private javax.swing.JLabel healTab;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JLayeredPane jLayeredPane3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton nextTurn;
    private javax.swing.JButton nextTurn1;
    private javax.swing.JButton nextTurn2;
    private javax.swing.JButton powerUpButton;
    private javax.swing.JLabel powerUpTab;
    private javax.swing.JTextField turnDisplay;
    private javax.swing.JLabel weaponOrType;
    // End of variables declaration//GEN-END:variables

    private void attack(BaseClass attacker, BaseClass toAttack) {
        int newHealth = 0;
         Random rn = new Random();
         double d = rn.nextDouble();
         if(attacker.getPlayerOrMonster() ==1 && d>0.8){
         JOptionPane.showMessageDialog(this,attacker.getName()+" attacks "+toAttack.getName()+ ". It’s not successful.");
         }else if(attacker.getPlayerOrMonster() ==2 && d>0.75){
         JOptionPane.showMessageDialog(this,attacker.getName()+" attacks "+toAttack.getName()+ ". It’s not successful.");
         }
          else if(attacker.getPlayerOrMonster() ==1){
            for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getName().equals(toAttack.getName())){
                if(attacker.getTypeOrWeapon().equalsIgnoreCase("Ice") && allPlayers.get(i).getTypeOrWeapon().equalsIgnoreCase("Water")){
                newHealth = toAttack.getHealth() - (2*attacker.getPower());
                if(newHealth < 1){
                    newHealth =0;
                }
                JOptionPane.showMessageDialog(this,attacker.getName()+ " attacked "+ toAttack.getTypeOrWeapon()+ " monster "+ toAttack.getName() +" with "+attacker.getTypeOrWeapon()+ " weapon for " + 2*attacker.getPower() + " damage. "+toAttack.getName()+" now has "+newHealth+" health");
                }
                else if(attacker.getTypeOrWeapon().equalsIgnoreCase("Fire") && allPlayers.get(i).getTypeOrWeapon().equalsIgnoreCase("Ice")){
                newHealth = toAttack.getHealth() - (2*attacker.getPower());
                if(newHealth < 1){
                    newHealth =0;
                }
                JOptionPane.showMessageDialog(this,attacker.getName()+ " attacked "+ toAttack.getTypeOrWeapon()+" monster "+ toAttack.getName() +" with "+attacker.getTypeOrWeapon()+ " weapon for " + 2*attacker.getPower() + " damage. "+toAttack.getName()+" now has "+newHealth+" health");
                }
                else if(attacker.getTypeOrWeapon().equalsIgnoreCase("Water") && allPlayers.get(i).getTypeOrWeapon().equalsIgnoreCase("Fire")){
                newHealth = toAttack.getHealth() - (2*attacker.getPower());
                    if(newHealth < 1){
                    newHealth =0;
                }
                JOptionPane.showMessageDialog(this,attacker.getName()+ " attacked "+ toAttack.getTypeOrWeapon()+" monster "+ toAttack.getName() +" with "+attacker.getTypeOrWeapon()+ " weapon for " + 2*attacker.getPower() + " damage. "+toAttack.getName()+" now has "+newHealth+" health");
                }else{
                 newHealth = toAttack.getHealth() - (attacker.getPower());
                    if(newHealth < 1){
                    newHealth =0;
                }
                JOptionPane.showMessageDialog(this,attacker.getName()+ " attacked monster "+ toAttack.getName()  +" for " + attacker.getPower() + " damage. "+toAttack.getName()+" now has "+newHealth+" health");
                }
                allPlayers.get(i).setHealth(newHealth);
                }
            }
        }else if(attacker.getPlayerOrMonster() ==2){
            for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getName().equals(toAttack.getName())){
                newHealth = toAttack.getHealth() - (attacker.getPower());
                if(newHealth < 1){
                    newHealth =0;
                }
                JOptionPane.showMessageDialog(this,attacker.getName()+ " attacked player "+ toAttack.getName() +" for " + attacker.getPower() + " damage. "+toAttack.getName()+" now has "+newHealth+" health");
                allPlayers.get(i).setHealth(newHealth);   
                } 
            }
        }
    }
    
    private void heal(BaseClass healer, BaseClass toheal) {
        int newHealth ;
         Random rn = new Random();
         double d = rn.nextDouble();
        if(healer.getId() == toheal.getId()){
            JOptionPane.showMessageDialog(this,"Don't be so greedy. Can't heal yourself. Please choose other player.");
        }else if(toheal.getLife() <= toheal.getHealth()){
            JOptionPane.showMessageDialog(this,toheal.getName()+" already have maximum health. Please choose other player. ");
        }
        else if(d>0.5){
         JOptionPane.showMessageDialog(this,healer.getName()+" ties to heal "+toheal.getName()+ ". It’s not successful.");
         turnCounter++;
        }else{
            for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getName().equals(toheal.getName())){
            newHealth = healer.getPower() + toheal.getHealth();
            if(newHealth >= toheal.getLife()){
                newHealth = toheal.getLife();
            }
            allPlayers.get(i).setHealth(newHealth);
            JOptionPane.showMessageDialog(this,healer.getName()+" healed "+toheal.getName()+" for "+healer.getPower()+" health.");
             }
            }
            turnCounter++;
        }
    }
    
     private void powerUp(BaseClass player) {
         int newPower;
         int newSpeed;
         Random rn = new Random();
         double d = rn.nextDouble();
         if(d>0.75){
         JOptionPane.showMessageDialog(this,player.getName()+" ties to Power Up. It’s not successful.");
         }else{
         for(int i=0;i<allPlayers.size();i++){
            if(allPlayers.get(i).getName().equals(player.getName())){
            newPower = player.getPower()*2;
            long roundedValue = Math.round((double) player.getSpeed() / 2);
            newSpeed = (int) roundedValue;
            allPlayers.get(i).setPower(newPower);
            allPlayers.get(i).setSpeed(newSpeed);
            JOptionPane.showMessageDialog(this,player.getName()+" uses Power Up. "+player.getName()+" now has "+ newSpeed + " speed and " + newPower + " power.");
             }
            }
         }
    }
     
     private void close() {
        WindowEvent closeWindow = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closeWindow);
    }
}

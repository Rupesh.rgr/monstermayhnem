/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.samitaphuyal.monstermayhnem.model;

/**
 *
 * @author Acer
 */
public class BaseClass {
    int id;
    String name;
    int power;
    int life;
    int speed;
    int health;
    int playerOrMonster;
    String typeOrWeapon;
    
    
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public void setTypeOrWeapon(String typeOrWeapon){
        this.typeOrWeapon = typeOrWeapon;
    }
    public String getTypeOrWeapon(){
        return typeOrWeapon;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setPower(int power){
        this.power = power;
    }
    public int getPower(){
        return power;
    }
     public void setHealth(int health){
        this.health = health;
    }
    public int getHealth(){
        return health;
    }
    public void setLife(int life){
        this.life = life;
    }
    public int getLife(){
        return life;
    }
    public void setSpeed(int speed){
        this.speed = speed;
    }
    public int getSpeed(){
        return speed;
    }
    public void setPlayerOrMonster(int playerOrMonster){
        this.playerOrMonster = playerOrMonster;
    }
    public int getPlayerOrMonster(){
        return playerOrMonster;
    }
    
}
